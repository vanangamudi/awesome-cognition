# AWESOME-COGNITION


[*Comparison table of Cognitive architectures*] (http://bicasociety.org/cogarch/architectures.php)


## Introduction


- [Wikipedia - Artificial General Intelligence](https://en.wikipedia.org/wiki/Artificial_general_intelligence)
- [Scholarpedia - AGI](http://www.scholarpedia.org/article/Artificial_General_Intelligence)
- [Goertzel's *Artificial General Intelligence: Concept, State of the Art and Future Prospects*](https://www.degruyter.com/view/j/jagi.ahead-of-print/jagi-2014-0001/jagi-2014-0001.xml)
- [Pei Wang's *A Gentle Introduction to AGI*](https://sites.google.com/site/narswang/home/agi-introduction)
- [Goertzel's *Contemporary Approaches to AGI*](http://www.springer.com/cda/content/document/cda_downloaddocument/9783540237334-c1.pdf?SGWID=0-0-45-330089-p43950079)
- [Goertzel's *Aspects of AGI*](http://sites.google.com/site/narswang/publications/wang-goertzel.AGI_Aspects.pdf)
- [Mapping the Landscape of Human-Level Artificial General Intelligence](http://www.aaai.org/ojs/index.php/aimagazine/article/view/2322)


## Guidelines/Philosophy

- [ ] Ray Kurzweil's **How to Create a Mind**


## Papers


### Deep Learning Community

- [ ] **Mikolov's** [A Roadmap towards Machine Intelligence](https://arxiv.org/abs/1511.08130)
    - The development of intelligent machines is one of the biggest unsolved challenges in computer science. 
    - In this paper, we propose some fundamental properties these machines should have, focusing in particular on communication and learning. 
    - We discuss a simple environment that could be used to incrementally teach a machine the basics of natural-language-based communication, 
    - as a prerequisite to more complex interaction with human users. 
    - We also present some conjectures on the sort of algorithms the machine should support in order to profitably learn from the environment.

------------------

## Benchmark Tasks


### Natural Language Understanding

- [**bAbI**](https://research.fb.com/projects/babi/)
    - Classic Language Modeling (Penn TreeBank, Text8)
    - Story understanding (Children’s Book Test, News articles)
    - Open Question Answering (WebQuestions, WikiQA)
    - Goal-Oriented Dialog and Chit-Chat (Movie Dialog)
    
- [DeepMind Q&A Dataset](http://cs.nyu.edu/~kcho/DMQA/)
- [Ubuntu Dialogue Corpus](https://github.com/rkadlec/ubuntu-ranking-dataset-creator)

------------------


## Unparsed

1. [OpenCog Publications] (http://wiki.opencog.org/w/Background_Publications)
2. [Kristinn R Thorisson - Selected Publications] (http://alumni.media.mit.edu/~kris/select_publ.html)
3. [AAAI Archives] (http://www.aaai.org/ojs/index.php/aimagazine/issue/archive)
4. [Goertzel Plan for AGI syllabus] (http://wp.goertzel.org/agi-curriculum/)
5. [AGI Society](http://www.agi-society.org/resources/)

------------------

## Contributors

[suriyadeepan](https://gitlab.com/suriyadeepan), [vanangamudi](https://gitlab.com/vanangamudi)
