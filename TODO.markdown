# TODO

- [ ] Need direct link and short description, to each paper, organised under a category - for example, a link to the book *PLN* should be under the category *OpenCog Publications*, with a description

> A probabilistic logic network (PLN) is a novel conceptual, mathematical and computational approach to uncertain inference; inspired by logic programming, but using probabilities in place of crisp (true/false) truth values, and fractional uncertainty in place of crisp known/unknown values.

- [ ] Pictures can express ideas quite efficiently. We need a folder for hosting the important block diagrams, etc, along with a common README file describing all pictures. They can be straight out of a paper or created by us, as we read the textual content.

- [ ] Organize resources, also based on type of content - paper, blog, video, etc,.

- [ ] Organize resources, also based on conceptual type - reasoning, memory, perception, etc,.

